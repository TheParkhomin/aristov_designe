// подключаем модули гапл
const gulp = require('gulp');
const gulp_concat = require('gulp-concat');
const gulp_autoprefixer = require('gulp-autoprefixer');
const gulp_clean_css = require('gulp-clean-css')
const gulp_sass = require('gulp-sass');
const del = require('del');

const css_files_path = [
    './app/static/src/css/**/*.css',
];

const sass_files_path = [
    './app/static/src/sass/**/*.scss'
];

const js_files = [
    './app/static/src/js/**/*.js'
];

const fonts_files = [
    './app/static/src/fonts/fonts/*.css'
];

const mobile_css_files_path = [
    './app/static/src/mobile/css/**/*.css',
];

const mobile_sass_files_path = [
    './app/static/src/mobile/sass/**/*.scss'
];

const mobile_js_files = [
    './app/static/src/mobile/js/**/*.js'
];

const mobile_fonts_files = [
    './app/static/src/mobile/fonts/fonts/*.css'
];

function styles() {
    return gulp.src(css_files_path)
        .pipe(gulp_concat('main.css'))
        .pipe(gulp_autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp_clean_css({
            level: 2
        }))
        .pipe(gulp.dest('./app/static/build/css/common'));
}

function mobile_styles() {
    return gulp.src(mobile_css_files_path)
        .pipe(gulp_concat('main.css'))
        .pipe(gulp_autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp_clean_css({
            level: 2
        }))
        .pipe(gulp.dest('./app/static/build/mobile/css/common'));
}

function fonts() {
    return gulp.src(fonts_files)
        .pipe(gulp_concat('fonts.css'))
        .pipe(gulp.dest('./app/static/src/fonts'));
}

function mobile_fonts() {
    return gulp.src(mobile_fonts_files)
        .pipe(gulp_concat('fonts.css'))
        .pipe(gulp.dest('./app/static/src/mobile/fonts'));
}

function dev_styles() {
    return gulp.src(css_files_path)
        .pipe(gulp_concat('main.css'))
        .pipe(gulp_autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/static/dev_build/css/common'));
}

function mobile_dev_styles() {
    return gulp.src(mobile_css_files_path)
        .pipe(gulp_concat('main.css'))
        .pipe(gulp_autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/static/dev_build/mobile/css/common'));
}

function sass_bem_on_css_bem() {
    return gulp.src(sass_files_path)
        .pipe(gulp_sass())
        .pipe(gulp.dest('./app/static/src/css'));
}

function mobile_sass_bem_on_css_bem() {
    return gulp.src(mobile_sass_files_path)
        .pipe(gulp_sass())
        .pipe(gulp.dest('./app/static/src/mobile/css'));
}

function scripts() {
    return gulp.src(js_files)
        .pipe(gulp_concat('scripts.js'))
        .pipe(gulp.dest('./app/static/build/js/common'));
}

function mobile_scripts() {
    return gulp.src(mobile_js_files)
        .pipe(gulp_concat('scripts.js'))
        .pipe(gulp.dest('./app/static/build/mobile/js/common'));
}

function clean() {
    return del(['app/static/src/css/*', 'app/static/build/css/common/*', 'app/static/build/js/common/*', 'app/static/dev_build/css/common/*', 'app/static/dev_build/js/common/*', 'app/static/build/css/*.css']);
}

function mobile_clean() {
    return del(['app/static/src/mobile/css/*', 'app/static/build/mobile/css/common/*', 'app/static/build/mobile/js/common/*', 'app/static/dev_build/mobile/css/common/*', 'app/static/dev_build/mobile/js/common/*', 'app/static/build/mobile/css/*.css']);
}

function watch() {
    gulp.watch('./app/static/src/sass/**/*.scss', sass_bem_on_css_bem);
    gulp.watch('./app/static/src/fonts/fonts/*.css', fonts);
    gulp.watch('./app/static/src/css/**/*.css', styles);
    gulp.watch('./app/static/src/css/**/*.css', dev_styles);
    gulp.watch('./app/static/src/js/**/*.js', scripts);

    gulp.watch('./app/static/src/mobile/sass/**/*.scss', mobile_sass_bem_on_css_bem);
    gulp.watch('./app/static/src/mobile/fonts/fonts/*.css', mobile_fonts);
    gulp.watch('./app/static/src/mobile/css/**/*.css', mobile_styles);
    gulp.watch('./app/static/src/mobile/css/**/*.css', mobile_dev_styles);
    gulp.watch('./app/static/src/mobile/js/**/*.js', mobile_scripts);
}

gulp.task('test_styles_gulp', styles);
gulp.task('test_sass_bem_on_css_bem', sass_bem_on_css_bem);
gulp.task('test_clean', clean);
gulp.task('test_fonts_gulp', fonts);
gulp.task('test_dev_styles_gulp', dev_styles);
gulp.task('test_script_gulp', scripts);


gulp.task('watch_gulp', watch);
gulp.task('build_gulp', gulp.series(clean, fonts, sass_bem_on_css_bem, dev_styles, gulp.parallel(styles, scripts)));
gulp.task('dev_gulp', gulp.series('build_gulp', 'watch_gulp'));