import smtplib

from django.shortcuts import render, HttpResponse, get_object_or_404
from email.mime.text import MIMEText
from email.header import Header

from django.conf import settings
from app.models import Work


def get_template(request, template):
    if request.user_agent.is_mobile:
        template = 'mobile/{}'.format(template)
    return template


def main(request):
    works = list(Work.objects.filter(for_main=True).order_by('-id'))
    main_work = next_work = next2_work = prev_work = None
    if len(works) >= 4 and not request.user_agent.is_mobile:
        main_work = works.pop()
        next_work = works.pop()
        next2_work = works.pop()
        prev_work = works[0]

        works.reverse()
        works.pop()

    return render(request, get_template(request, 'main.html'),
                  {
                      'works': works, 'main_work': main_work, 'next_work': next_work,
                      'next2_work': next2_work, 'prev_work': prev_work
                  }
                  )


def portfolio(request):
    works = Work.objects.order_by('order')
    return render(request, get_template(request, 'portfolio.html'), {'works': works})


def about(request):
    return render(request, get_template(request, 'about.html'))


def send_offer(request):
    name = request.POST.get('name')

    if not name:
        return HttpResponse('Not name')
    email = request.POST.get('email')
    phone = request.POST.get('phone')
    text = request.POST.get('text')
    if not (email or phone):
        return HttpResponse('Not email of phones')

    offer = 'Имя: {} \n Email: {} \n Tелефон: {}'.format(name, email, phone)
    if not settings.DEBUG:
        smtpObj = smtplib.SMTP('smtp.yandex.ru')
        smtpObj.starttls()
        smtpObj.login(settings.EMAIL_LOGIN, settings.EMAIL_PASSWORD)

        msg = MIMEText(offer, 'plain', 'utf-8')
        msg['Subject'] = Header('Новая заявка', 'utf-8')
        msg['From'] = settings.EMAIL_LOGIN
        msg['To'] = settings.EMAIL_OFFER

        smtpObj.sendmail(msg['From'], msg['To'], msg.as_string())
        smtpObj.quit()

    return HttpResponse('ok')
