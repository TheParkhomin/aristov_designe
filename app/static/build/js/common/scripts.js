$(document).ready(function () {
    let popup = $('.contacts-popup__wrap');
    $('.header__menu-item__contacts').click(function () {
        popup.removeClass('contacts-popup__wrap_none');
    });

    $('.contacts-popup__esc').click(function () {
        popup.addClass('contacts-popup__wrap_none');
    })
});

$(document).ready(function () {
    let ancor = window.location.href.match(/#.*/g);
    if (ancor[0]) {
        var id  = $(ancor[0].replace('_', '')),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    }
});
$(document).ready(function () {
    let header = $('.header__main');
    header_height();

    window.addEventListener('scroll', function () {
        header_height();
    });

    function header_height() {
        if (pageYOffset != 0) {
            header.addClass('header__main_activate');
        } else {
            header.removeClass('header__main_activate');
        }
    }

    $(".header__contactbtn_main").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $(".header__menu-item_ankor").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});
$(document).ready(function () {

    let cursor = $('.main-window__slider-info-text-cursor');
    let title_text = $('.main-window__slider-info-text');
    let link = $('.main-window__slider-info-link');
    let prev_btn = $('.main-window__slider-prevbtn');
    title_text.text($('.main-window__slider-item_main').data('title'));

    $(document).on('click', '.main-window__slider-item_next', function () {
        next();
    });

    $('.main-window__slider-prevbtn').click(function () {

        let main_slider = $('.main-window__slider-item_main');
        let next_slider = $('.main-window__slider-item_next');
        let next2 = $('.main-window__slider-item_next2');
        let prev = $('.main-window__slider-item_prev');
        let main_slide = $('.main-window__slider-item_main');

        prev_btn.addClass('main-window__slider-prevbtn_noevents');
        main_slide.addClass('main-window__slider-prevbtn_noevents');

        create_typed($('.main-window__slider-info-text'), prev.data('title'), 'prev', prev_btn, main_slide);
        change_link(prev.data('href'));

        let prev_prev = prev.prev();

        if (!prev_prev[0]) {
            prev_prev = $('.main-window__slider').children().last();
        }

        next_slider.off('mouseenter', on_animate);
        next_slider.off('mouseleave', off_animate);
        main_slider.mouseenter(on_animate).mouseleave(off_animate);

        main_slider.addClass('main-window__slider-item_main-right');

        prev.addClass('main-window__slider-item_prev-back');

        setTimeout(function () {
            main_slider.addClass('main-window__slider-item_next');
            main_slider.removeClass('main-window__slider-item_main');
            main_slider.removeClass('main-window__slider-item_main-right');

            prev.addClass('main-window__slider-item_main');
            prev.removeClass('main-window__slider-item_prev-back');
            prev.removeClass('main-window__slider-item_prev');

            prev_prev.addClass('main-window__slider-item_prev');

            next_slider.addClass('main-window__slider-item_next2');
            next_slider.removeClass('main-window__slider-item_next');

            next2.removeClass('main-window__slider-item_next2');

        }, 1000)
    });

    function create_typed(elem, text_new, next2, next_next, next_slider) {
        let time = 1;

        for(let i = elem.text().length; i >= 0; i--) {
            setTimeout(function() {
                elem.text(elem.text().slice(0, -1));
                if (i === 0) {
                    setTimeout(function () {
                        for(let j = 0; j < text_new.length; j++) {
                            setTimeout(function () {
                                elem.text(elem.text() + text_new[j]);
                                if (next2 === 'prev') {
                                    if (j + 1 === text_new.length) {
                                        setTimeout(function () {
                                            next_slider.removeClass('main-window__slider-prevbtn_noevents');
                                            next_next.removeClass('main-window__slider-prevbtn_noevents');
                                        }, 500)
                                        elem.text(text_new);
                                    }
                                } else {
                                    if (j + 1 === text_new.length) {
                                        setTimeout(function () {
                                            emened (next2, next_next);
                                            prev_btn.removeClass('main-window__slider-prevbtn_noevents');
                                            next_slider.removeClass('main-window__slider-prevbtn_noevents');
                                        }, 500)
                                        elem.text(text_new);
                                    }
                                }
                            }, 30 * j);
                        }
                    }, 500);
                }
            }, 70 * time++)
        }
    }

    function emened (next2, next_next) {
        next2.removeClass('main-window__slider-item-noevents');
        next_next.addClass('main-window__slider-item_next2');
    }

    function change_link (variable) {
        link.attr('href', variable);
    }

    function next() {
        let main_slider = $('.main-window__slider-item_main');
        let next_slider = $('.main-window__slider-item_next');
        let next2 = $('.main-window__slider-item_next2');
        let next_next = next2.next();

        prev_btn.addClass('main-window__slider-prevbtn_noevents');
        next_slider.addClass('main-window__slider-prevbtn_noevents');

        if (!next_next[0]) {
            next_next = $('.main-window__slider').children().first();
        }
        main_slider.addClass('main-window__slider-item_main-left');

        next_slider.addClass('main-window__slider-item_next-left');

        create_typed($('.main-window__slider-info-text'), next_slider.data('title'), next2, next_next, next_slider);
        change_link(next_slider.data('href'));

        setTimeout(function () {
            let prev = $('.main-window__slider-item_prev');

            main_slider.removeClass('main-window__slider-item_main');
            main_slider.removeClass('main-window__slider-item_main-left');
            main_slider.addClass('main-window__slider-item_prev');

            prev.removeClass('main-window__slider-item_prev');

            next_slider.addClass('main-window__slider-item_main');
            next_slider.removeClass('main-window__slider-item_next');
            next_slider.removeClass('main-window__slider-item_next-left');

            next2.addClass('main-window__slider-item_next');
            next2.removeClass('main-window__slider-item_next2');

            next2.addClass('main-window__slider-item-noevents');


            next_slider.off('mouseenter', on_animate);
            $('.main-window__slider-item_main-hover').removeClass('main-window__slider-item_main-hover');
            next_slider.off('mouseleave', off_animate);
            next2.mouseenter(on_animate).mouseleave(off_animate);

        }, 1000);
    };

    $('.main-window__slider-item_next').hover(on_animate, off_animate);
    $('.main-window__slider-nextbtn').hover(on_animate, off_animate);

    function on_animate() {
        $('.main-window__slider-item_main').addClass('main-window__slider-item_main-hover');
    }

    function off_animate() {
        $('.main-window__slider-item_main').removeClass('main-window__slider-item_main-hover');
    }
});
$(document).ready(function () {
    $('.portfolio__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000
    });
});
$(document).ready(function () {
    $('.services__pillar-item-wrap_title').hover( handlerIn1, handlerOut1 );

    function handlerIn1() {
        console.log(123);
        $('.services__pillar-bg').addClass('services__pillar-bg_animate');
    }
    function handlerOut1() {
        $('.services__pillar-bg').removeClass('services__pillar-bg_animate');
    }
});
$(document).ready(function () {
    $('.tnh-popup').click(function () {
        $('.tnh-popup__wrap').addClass('tnh-popup__wrap_close');
    })
});
$(document).ready(function () {
    $('.works__text-wrap').hover(funk_in, funk_out);
    $('.works__bg').hover(funk_in2, funk_out2);

    function funk_in() {
        let timerId = setInterval(function () {});
        let count_elem = $(this).find('.works__text-count');
        let count = count_elem.data('count');
        count_elem.text('0');
        count_elem.removeClass('works__text-count_activate ');
        count_elem.addClass('works__text-count_activate');
        activate_count(count_elem, count, timerId)
    }

    function funk_out() {}

    function activate_count(elem, count, timer) {
        setTimeout(function () {
            elem.removeClass('works__text-count_activate');
        }, 2250)

        timer = setInterval(function () {
            if (count > 2000) {
                elem.text(+elem.text() + 5);

                if (elem.text() >= count) {
                    clearInterval(timer);
                    elem.text(count);
                }
            } else {
                elem.text(+elem.text() + 1);

                if (elem.text() >= count) {
                    clearInterval(timer);
                    elem.text(count);
                }
            }
        }, 3000 / count);
    }

    function funk_in2() {
        $('.works__slider').attr('style', $(this).attr('style'));
    }

    function funk_out2() {}

});