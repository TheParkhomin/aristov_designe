$(document).ready(function () {
    let ancor = window.location.href.match(/#.*/g);
    if (ancor[0]) {
        var id  = $(ancor[0].replace('_', '')),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    }
});
$(document).ready(function () {
    $('.header__menu-item_form').click(function () {
        $('.contact-form__wrap').addClass('contact-form__wrap_open');
        setTimeout(function() {
            $('.contact-form').addClass('contact-form_open');
        }, 100)
    })
    $('.contact-form__esc').click(function () {
        $('.contact-form').removeClass('contact-form_open');
        setTimeout(function () {
            $('.contact-form__wrap').removeClass('contact-form__wrap_open');
        }, 700)
    })
});
$(document).ready(function () {
    let content = $('.content__wrap');
    let footer = $('.footer__wrap');

    let menu1 = $('.header__top-menu-btn-item_1');
    let menu2 = $('.header__top-menu-btn-item_2');
    let menu3 = $('.header__top-menu-btn-item_3');

    $('.header__top-menu-btn-wrap').click(function () {
        $('.header__menu').toggleClass('header__menu_open');
        content.toggleClass('content__wrap_open-menu');
        footer.removeClass('footer__wrap_top');

        if ($('.header__menu_open')[0]) {
            menu1.addClass('header__top-menu-btn-item__1');
            menu2.addClass('header__top-menu-btn-item__2');
            menu3.addClass('header__top-menu-btn-item__3');
        } else {
            menu1.removeClass('header__top-menu-btn-item__1');
            menu2.removeClass('header__top-menu-btn-item__2');
            menu3.removeClass('header__top-menu-btn-item__3');
        }
    });

    $('.header__menu-item_contacts').click(function () {
        footer.toggleClass('footer__wrap_top');
    })
});
$(document).ready(function () {
    let now_bg = '';
    let content = $('.info__item');
    window.addEventListener('scroll', function () {
        let centerX = 21;
        let centerY = document.documentElement.clientHeight / 2;

        let elem = document.elementFromPoint(centerX, centerY);
        if ($(elem).hasClass('info__content__bg')) {
            if (now_bg !== elem) {
                now_bg = elem;
                content.attr('style', $(elem).attr('style'));
            }
        }
    });

    $(document).on('touchstart', '.info__block-wrap', funk_in);
    
    function funk_in() {
        let timerId = setInterval(function () {});
        let count_elem = $(this).find('.info__block-count');
        let count = count_elem.data('count');
        count_elem.text('0');
        count_elem.removeClass('info__block-count_activate ');
        count_elem.addClass('info__block-count_activate');
        activate_count(count_elem, count, timerId)
    }

    function activate_count(elem, count, timer) {
        setTimeout(function () {
            elem.removeClass('info__block-count_activate');
        }, 2250)

        timer = setInterval(function () {
            elem.text(+elem.text() + 1);

            if (elem.text() >= count) {
                clearInterval(timer);
                elem.text(count);
            }
        }, 3000 / count);
    }
});
$(document).ready(function () {
    let link = $('.main-view__link');

    var slickOpts = {
        dots: true,
        customPaging: function (slick, index) {
            return '<a class="main-view__slider-pagin-item"></a>';
        }
    }

    $('.main-view__slider-wrap').slick(slickOpts);

    change_btn($('#slick-slide10').find('.main-view__slider-item-photo'));

    $('.main-view__slider-wrap').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        console.log(nextSlide);
        change_btn($('#slick-slide1' + nextSlide).find('.main-view__slider-item-photo'));
    });

    function change_btn (elem) {
        console.log(elem);
        link.removeClass('main-view__link_top');
        link.attr('href', '/portfolio/' + elem.data('href'))
        setTimeout(function () {
            link.addClass('main-view__link_top');
        }, 700);
    }
});
$(document).ready(function () {
    $('.portfolio__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000
    });
});
$(document).ready(function () {
    $(document).on('touchstart', '.services__item-title-info-btn', function (event) {
        let elem = $(this).closest('.services__item__wrap').find('.services__item-title-info');

        if (elem.hasClass('services__item-title-info_open')) {
            elem.removeClass('services__item-title-info_bg');
            setTimeout(function () {
                elem.removeClass('services__item-title-info_open')
            }, 700);
        } else {
            elem.addClass('services__item-title-info_open');
            setTimeout(function () {
                elem.addClass('services__item-title-info_bg')
            }, 700);
        }
    });
});