$(document).ready(function () {
    $(document).on('touchstart', '.services__item-title-info-btn', function (event) {
        let elem = $(this).closest('.services__item__wrap').find('.services__item-title-info');

        if (elem.hasClass('services__item-title-info_open')) {
            elem.removeClass('services__item-title-info_bg');
            setTimeout(function () {
                elem.removeClass('services__item-title-info_open')
            }, 700);
        } else {
            elem.addClass('services__item-title-info_open');
            setTimeout(function () {
                elem.addClass('services__item-title-info_bg')
            }, 700);
        }
    });
});