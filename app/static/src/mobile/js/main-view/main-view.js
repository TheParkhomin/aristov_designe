$(document).ready(function () {
    let link = $('.main-view__link');

    var slickOpts = {
        dots: true,
        customPaging: function (slick, index) {
            return '<a class="main-view__slider-pagin-item"></a>';
        }
    }

    $('.main-view__slider-wrap').slick(slickOpts);

    change_btn($('#slick-slide10').find('.main-view__slider-item-photo'));

    $('.main-view__slider-wrap').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        console.log(nextSlide);
        change_btn($('#slick-slide1' + nextSlide).find('.main-view__slider-item-photo'));
    });

    function change_btn (elem) {
        console.log(elem);
        link.removeClass('main-view__link_top');
        link.attr('href', '/portfolio/' + elem.data('href'))
        setTimeout(function () {
            link.addClass('main-view__link_top');
        }, 700);
    }
});