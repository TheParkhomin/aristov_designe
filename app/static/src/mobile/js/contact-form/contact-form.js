$(document).ready(function () {
    $('.header__menu-item_form').click(function () {
        $('.contact-form__wrap').addClass('contact-form__wrap_open');
        setTimeout(function() {
            $('.contact-form').addClass('contact-form_open');
        }, 100)
    })
    $('.contact-form__esc').click(function () {
        $('.contact-form').removeClass('contact-form_open');
        setTimeout(function () {
            $('.contact-form__wrap').removeClass('contact-form__wrap_open');
        }, 700)
    })
});