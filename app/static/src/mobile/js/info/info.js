$(document).ready(function () {
    let now_bg = '';
    let content = $('.info__item');
    window.addEventListener('scroll', function () {
        let centerX = 21;
        let centerY = document.documentElement.clientHeight / 2;

        let elem = document.elementFromPoint(centerX, centerY);
        if ($(elem).hasClass('info__content__bg')) {
            if (now_bg !== elem) {
                now_bg = elem;
                content.attr('style', $(elem).attr('style'));
            }
        }
    });

    $(document).on('touchstart', '.info__block-wrap', funk_in);
    
    function funk_in() {
        let timerId = setInterval(function () {});
        let count_elem = $(this).find('.info__block-count');
        let count = count_elem.data('count');
        count_elem.text('0');
        count_elem.removeClass('info__block-count_activate ');
        count_elem.addClass('info__block-count_activate');
        activate_count(count_elem, count, timerId)
    }

    function activate_count(elem, count, timer) {
        setTimeout(function () {
            elem.removeClass('info__block-count_activate');
        }, 2250)

        timer = setInterval(function () {
            elem.text(+elem.text() + 1);

            if (elem.text() >= count) {
                clearInterval(timer);
                elem.text(count);
            }
        }, 3000 / count);
    }
});