$(document).ready(function () {
    let ancor = window.location.href.match(/#.*/g);
    if (ancor[0]) {
        var id  = $(ancor[0].replace('_', '')),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    }
});