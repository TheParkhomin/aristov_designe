$(document).ready(function () {
    let content = $('.content__wrap');
    let footer = $('.footer__wrap');

    let menu1 = $('.header__top-menu-btn-item_1');
    let menu2 = $('.header__top-menu-btn-item_2');
    let menu3 = $('.header__top-menu-btn-item_3');

    $('.header__top-menu-btn-wrap').click(function () {
        $('.header__menu').toggleClass('header__menu_open');
        content.toggleClass('content__wrap_open-menu');
        footer.removeClass('footer__wrap_top');

        if ($('.header__menu_open')[0]) {
            menu1.addClass('header__top-menu-btn-item__1');
            menu2.addClass('header__top-menu-btn-item__2');
            menu3.addClass('header__top-menu-btn-item__3');
        } else {
            menu1.removeClass('header__top-menu-btn-item__1');
            menu2.removeClass('header__top-menu-btn-item__2');
            menu3.removeClass('header__top-menu-btn-item__3');
        }
    });

    $('.header__menu-item_contacts').click(function () {
        footer.toggleClass('footer__wrap_top');
    })
});