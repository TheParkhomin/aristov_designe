$(document).ready(function () {
    let header = $('.header__main');
    header_height();

    window.addEventListener('scroll', function () {
        header_height();
    });

    function header_height() {
        if (pageYOffset != 0) {
            header.addClass('header__main_activate');
        } else {
            header.removeClass('header__main_activate');
        }
    }

    $(".header__contactbtn_main").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $(".header__menu-item_ankor").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});