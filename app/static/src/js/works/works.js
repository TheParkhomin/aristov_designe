$(document).ready(function () {
    $('.works__text-wrap').hover(funk_in, funk_out);
    $('.works__bg').hover(funk_in2, funk_out2);

    function funk_in() {
        let timerId = setInterval(function () {});
        let count_elem = $(this).find('.works__text-count');
        let count = count_elem.data('count');
        count_elem.text('0');
        count_elem.removeClass('works__text-count_activate ');
        count_elem.addClass('works__text-count_activate');
        activate_count(count_elem, count, timerId)
    }

    function funk_out() {}

    function activate_count(elem, count, timer) {
        setTimeout(function () {
            elem.removeClass('works__text-count_activate');
        }, 2250)

        timer = setInterval(function () {
            if (count > 2000) {
                elem.text(+elem.text() + 5);

                if (elem.text() >= count) {
                    clearInterval(timer);
                    elem.text(count);
                }
            } else {
                elem.text(+elem.text() + 1);

                if (elem.text() >= count) {
                    clearInterval(timer);
                    elem.text(count);
                }
            }
        }, 3000 / count);
    }

    function funk_in2() {
        $('.works__slider').attr('style', $(this).attr('style'));
    }

    function funk_out2() {}

});