$(document).ready(function () {
    $('.portfolio__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 4000
    });
});