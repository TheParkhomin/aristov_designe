$(document).ready(function () {
    let popup = $('.contacts-popup__wrap');
    $('.header__menu-item__contacts').click(function () {
        popup.removeClass('contacts-popup__wrap_none');
    });

    $('.contacts-popup__esc').click(function () {
        popup.addClass('contacts-popup__wrap_none');
    })
});