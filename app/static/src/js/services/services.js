$(document).ready(function () {
    $('.services__pillar-item-wrap_title').hover( handlerIn1, handlerOut1 );

    function handlerIn1() {
        $('.services__pillar-bg').addClass('services__pillar-bg_animate');
    }
    function handlerOut1() {
        $('.services__pillar-bg').removeClass('services__pillar-bg_animate');
    }
});