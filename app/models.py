import os
from PIL import Image as Image_pil

from django.db import models


class Work(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    translit = models.CharField(max_length=255, verbose_name='Транслит')
    photo = models.ImageField(verbose_name='Фото')
    photo_mobile = models.ImageField(verbose_name='Фото для мобильной версии', blank=True, null=True)
    style = models.CharField(max_length=100, verbose_name='Стиль работы', blank=True, null=True)
    for_main = models.BooleanField(default=False, verbose_name='Для главной страницы')
    order = models.IntegerField(verbose_name='Порядок вывода',  null=True, blank=True)

    class Meta:
        verbose_name = 'Работа'
        verbose_name_plural = 'Работы'
        ordering = ['order']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.pk:
            last_works = Work.objects.filter(order__isnull=False)
            for work in last_works:
                work.order += 1
                work.save()
            self.order = 1

        super().save(*args, **kwargs)
        Image_pil.open(self.photo.path).save(self.photo.path, quality=50)
        os.chmod(self.photo.path, 436)


class Image(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    image = models.ImageField(verbose_name='Изображение')
    work = models.ForeignKey(Work, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Изображение работы'
        verbose_name_plural = 'Изображения работ'

    def __str__(self):
        return '{} {}'.format(self.name, self.work)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        Image_pil.open(self.image.path).save(self.image.path, quality=50)
        os.chmod(self.image.path, 436)


class Phone(models.Model):
    number = models.CharField(max_length=255, verbose_name='Телефонный номер')
    is_active = models.BooleanField(verbose_name='Активный номер', default=False)

    class Meta:
        verbose_name = 'Номер телефона'
        verbose_name_plural = 'Номера телефонов'

    def __str__(self):
        return self.number


class Facebook(models.Model):
    url = models.CharField(max_length=255, verbose_name='Ссылка на fb')
    is_active = models.BooleanField(verbose_name='Активная ссылка', default=False)

    class Meta:
        verbose_name = 'Ссылка на FB'
        verbose_name_plural = 'Ссылки на FB'

    def __str__(self):
        return self.url


class Instagram(models.Model):
    url = models.CharField(max_length=255, verbose_name='Ссылка на instagram')
    is_active = models.BooleanField(verbose_name='Активная ссылка', default=False)

    class Meta:
        verbose_name = 'Ссылка на Instagram'
        verbose_name_plural = 'Ссылки на Instagram'

    def __str__(self):
        return self.url


class Mail(models.Model):
    email = models.CharField(max_length=255, verbose_name='Почта')
    is_active = models.BooleanField(verbose_name='Активная почта', default=False)

    class Meta:
        verbose_name = 'Адрес почты'
        verbose_name_plural = 'Адреса почты'

    def __str__(self):
        return self.email
