# Generated by Django 2.2.4 on 2019-10-25 19:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_work_style'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='work',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Work'),
        ),
    ]
