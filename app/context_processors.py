from app.models import Phone, Facebook, Instagram, Mail


def phone(request):
    phone_number = Phone.objects.filter(is_active=True).last()
    if not phone_number:
        phone_number = '+7 918 670-71-73'

    return {'phone': phone_number}


def fb(request):
    fb_url = Facebook.objects.filter(is_active=True).last()
    if not fb_url:
        fb_url = 'https://www.facebook.com/ArstvDsgn?ref=bookmarks'

    return {'fb': fb_url}


def instagram(request):
    inst_url = Instagram.objects.filter(is_active=True).last()
    if not inst_url:
        inst_url = 'https://www.instagram.com/aristovdesign/'

    return {'instagram': inst_url}


def mail(request):
    email = Mail.objects.filter(is_active=True).last()
    if not email:
        email = 'info@aristovdesign.ru'

    return {'mail': email}

