from django.contrib import admin
from app.models import Work, Image, Phone, Facebook, Instagram, Mail

admin.site.register(Phone)
admin.site.register(Facebook)
admin.site.register(Instagram)
admin.site.register(Mail)


class ImageInline(admin.TabularInline):
    model = Image
    extra = 1


class WorkAdmin(admin.ModelAdmin):
    inlines = [
        ImageInline
    ]


admin.site.register(Work, WorkAdmin)
